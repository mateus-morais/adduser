# Portuguese translation of adduser
# Copyright (C) 2000 Cesar Eduardo Barros
# Cesar Eduardo Barros <cesarb@web4u.com.br>, 2000.
# André Luís Lopes <andreop@debian.org>, 2004.
# Éverton Arruda <root@earruda.eti.br>, 2010.
# Adriano Rafael Gomes <adrianorg@arg.eti.br>, 2010-2016.
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
msgid ""
msgstr ""
"Project-Id-Version: adduser 3.105\n"
"Report-Msgid-Bugs-To: adduser@packages.debian.org\n"
"POT-Creation-Date: 2023-06-09 12:08+0200\n"
"PO-Revision-Date: 2016-06-17 18:20+0200\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@arg.eti.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../adduser:190
msgid "Only root may add a user or group to the system."
msgstr "Somente root pode acrescentar um usuário ou grupo ao sistema."

#: ../adduser:227 ../deluser:176
msgid "Only one or two names allowed."
msgstr "Somente um ou dois nomes permitidos."

#: ../adduser:234
msgid "Specify only one name in this mode."
msgstr "Especifique somente um nome neste modo."

#: ../adduser:238
msgid "addgroup with two arguments is an unspecified operation."
msgstr ""

#: ../adduser:263
msgid "The --group, --ingroup, and --gid options are mutually exclusive."
msgstr "As opções --group, --ingroup e --gid são mutuamente exclusivas."

#: ../adduser:269
msgid "The home dir must be an absolute path."
msgstr "O diretório pessoal deve ser um caminho absoluto."

#: ../adduser:275
#, fuzzy, perl-format
#| msgid "Warning: The home dir %s you specified already exists."
msgid "The home dir %s you specified already exists.\n"
msgstr "Alerta: o diretório pessoal %s que você especificou já existe."

#: ../adduser:278
#, fuzzy, perl-format
#| msgid "Warning: The home dir %s you specified can't be accessed: %s"
msgid "The home dir %s you specified can't be accessed: %s\n"
msgstr ""
"Alerta: o diretório pessoal %s que você especificou não pode ser acessado: "
"%s\n"

#: ../adduser:347
#, perl-format
msgid "The group `%s' already exists as a system group. Exiting."
msgstr "O grupo '%s' já existe como um grupo de sistema. Saindo."

#: ../adduser:352
#, perl-format
msgid "The group `%s' already exists and is not a system group. Exiting."
msgstr "O grupo '%s' já existe e não é um grupo de sistema. Saindo."

#: ../adduser:357
#, perl-format
msgid "The group `%s' already exists, but has a different GID. Exiting."
msgstr "O grupo '%s' já existe, mas tem um GID diferente. Saindo."

#: ../adduser:362 ../adduser:399
#, perl-format
msgid "The GID `%s' is already in use."
msgstr "O GID '%s' já está em uso."

#: ../adduser:373
#, fuzzy, perl-format
#| msgid ""
#| "No GID is available in the range %d-%d (FIRST_SYS_GID - LAST_SYS_GID).\n"
msgid "No GID is available in the range %d-%d (FIRST_SYS_GID - LAST_SYS_GID)."
msgstr ""
"Nenhum GID está disponível na faixa %d-%d (FIRST_SYS_GID - LAST_SYS_GID).\n"

#: ../adduser:375 ../adduser:418
#, perl-format
msgid "The group `%s' was not created."
msgstr "O grupo '%s' não foi criado."

#: ../adduser:381 ../adduser:423
#, perl-format
msgid "Adding group `%s' (GID %d) ..."
msgstr "Adicionando grupo '%s' (GID %d) ..."

#: ../adduser:395 ../adduser:1081
#, perl-format
msgid "The group `%s' already exists."
msgstr "O grupo '%s' já existe."

#: ../adduser:416
#, perl-format
msgid "No GID is available in the range %d-%d (FIRST_GID - LAST_GID)."
msgstr "Nenhum GID está disponível na faixa %d-%d (FIRST_GID - LAST_GID)."

#: ../adduser:437 ../deluser:266
#, perl-format
msgid "The user `%s' does not exist."
msgstr "O usuário '%s' não existe."

#: ../adduser:441 ../adduser:857 ../adduser:1091 ../deluser:408 ../deluser:411
#, perl-format
msgid "The group `%s' does not exist."
msgstr "O grupo '%s' não existe."

#: ../adduser:446 ../adduser:861
#, perl-format
msgid "The user `%s' is already a member of `%s'."
msgstr "O usuário '%s' já é um membro de '%s'."

#: ../adduser:450 ../adduser:867
#, perl-format
msgid "Adding user `%s' to group `%s' ..."
msgstr "Adicionando usuário '%s' ao grupo '%s' ..."

#: ../adduser:469
#, fuzzy, perl-format
#| msgid "The system user `%s' already exists. Exiting."
msgid "The system user `%s' already exists. Exiting.\n"
msgstr "O usuário de sistema '%s' já existe. Saindo."

#: ../adduser:472
#, fuzzy, perl-format
#| msgid "The user `%s' already exists, and is not a system user."
msgid "The user `%s' already exists, but is not a system user. Exiting."
msgstr "O usuário '%s' já existe, e não é um usuário de sistema."

#: ../adduser:476
#, perl-format
msgid "The user `%s' already exists with a different UID. Exiting."
msgstr "O usuário '%s' já existe com um UID diferente. Saindo."

#: ../adduser:490
#, fuzzy, perl-format
#| msgid ""
#| "No UID/GID pair is available in the range %d-%d (FIRST_SYS_UID - "
#| "LAST_SYS_UID).\n"
msgid ""
"No UID/GID pair is available in the range %d-%d (FIRST_SYS_UID - "
"LAST_SYS_UID)."
msgstr ""
"Nenhum par UID/GID está disponível na faixa %d-%d (FIRST_SYS_UID - "
"LAST_SYS_UID).\n"

#: ../adduser:493 ../adduser:509 ../adduser:591 ../adduser:705 ../adduser:711
#, perl-format
msgid "The user `%s' was not created."
msgstr "O usuário '%s' não foi criado."

#: ../adduser:506
#, fuzzy, perl-format
#| msgid ""
#| "No UID is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID).\n"
msgid "No UID is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID)."
msgstr ""
"Nenhum UID está disponível na faixa %d-%d (FIRST_SYS_UID - LAST_SYS_UID).\n"

#: ../adduser:517 ../adduser:529
#, fuzzy
#| msgid "Internal error"
msgid "Internal error"
msgstr "Erro interno"

#: ../adduser:533
#, perl-format
msgid "Adding system user `%s' (UID %d) ..."
msgstr "Adicionando usuário de sistema '%s' (UID %d) ..."

#: ../adduser:537
#, perl-format
msgid "Adding new group `%s' (GID %d) ..."
msgstr "Adicionando novo grupo '%s' (GID %d) ..."

#: ../adduser:543
#, perl-format
msgid "Adding new user `%s' (UID %d) with group `%s' ..."
msgstr "Adicionando novo usuário '%s' (UID %d) com grupo '%s' ..."

#: ../adduser:590
msgid ""
"USERS_GID and USERS_GROUP both given in configuration. This is an error."
msgstr ""

#: ../adduser:681
#, perl-format
msgid "Adding user `%s' ..."
msgstr "Adicionando usuário '%s' ..."

#: ../adduser:704
#, fuzzy, perl-format
#| msgid ""
#| "No UID/GID pair is available in the range %d-%d (FIRST_UID - LAST_UID).\n"
msgid "No UID/GID pair is available in the range %d-%d (FIRST_UID - LAST_UID)."
msgstr ""
"Nenhum par UID/GID está disponível na faixa %d-%d (FIRST_UID - LAST_UID).\n"

#: ../adduser:710
msgid ""
"USERGROUPS=no, USER_GID=-1 and USERS_GROUP empty. A user needs a primary "
"group!"
msgstr ""

#: ../adduser:750
msgid "Internal error interpreting parameter combination"
msgstr ""

#: ../adduser:760
#, perl-format
msgid "Adding new group `%s' (%d) ..."
msgstr "Adicionando novo grupo '%s' (%d) ..."

#: ../adduser:763
#, fuzzy, perl-format
#| msgid "Adding new group `%s' (GID %d) ..."
msgid "Adding new group `%s' (new group ID) ..."
msgstr "Adicionando novo grupo '%s' (GID %d) ..."

#: ../adduser:766
#, perl-format
msgid "new group '%s' created with GID %d"
msgstr ""

#: ../adduser:773
#, fuzzy, perl-format
#| msgid "Adding new user `%s' (%d) with group `%s' ..."
msgid "Adding new user `%s' (%d) with group `%s (%d)' ..."
msgstr "Adicionando novo usuário '%s' (%d) com grupo '%s' ..."

#. hm, error, should we break now?
#: ../adduser:802
msgid "Permission denied"
msgstr "Permissão negada"

#: ../adduser:803
msgid "invalid combination of options"
msgstr "combinação de opções inválida"

#: ../adduser:804
msgid "unexpected failure, nothing done"
msgstr "falha inesperada, nada a ser feito"

#: ../adduser:805
msgid "unexpected failure, passwd file missing"
msgstr "falha inesperada, arquivo passwd faltando"

#: ../adduser:806
msgid "passwd file busy, try again"
msgstr "arquivo passwd ocupado, tente novamente"

#: ../adduser:807
msgid "invalid argument to option"
msgstr "argumento invalido para opção"

#: ../adduser:813
msgid "Try again? [y/N] "
msgstr "Tentar novamente? [s/N] "

#: ../adduser:839
msgid "Is the information correct? [Y/n] "
msgstr "A informação está correta? [S/n] "

#: ../adduser:853
#, fuzzy, perl-format
#| msgid "Adding new user `%s' to extra groups ..."
msgid "Adding new user `%s' to supplemental / extra groups `%s' ..."
msgstr "Adicionando novo usuário '%s' a grupos extra ..."

#: ../adduser:877
#, perl-format
msgid "Setting quota for user `%s' to values of user `%s' ..."
msgstr "Definindo cota para usuário '%s' para valores de usuário '%s' ..."

#: ../adduser:915
#, fuzzy, perl-format
#| msgid "Not creating home directory `%s'."
msgid "Not creating `%s'."
msgstr "Não criando diretório pessoal '%s'."

#: ../adduser:917
#, perl-format
msgid "Not creating home directory `%s'."
msgstr "Não criando diretório pessoal '%s'."

#: ../adduser:919
#, fuzzy, perl-format
#| msgid "The home directory `%s' already exists.  Not copying from `%s'."
msgid "The home directory `%s' already exists.  Not touching this directory."
msgstr "O diretório pessoal '%s' já existe. Não copiando de '%s'."

#: ../adduser:925
#, fuzzy, perl-format
#| msgid ""
#| "Warning: The home directory `%s' does not belong to the user you are "
#| "currently creating.\n"
msgid ""
"Warning: The home directory `%s' does not belong to the user you are "
"currently creating."
msgstr ""
"Alerta: o diretório pessoal '%s' não pertence ao usuário que você está "
"criando atualmente.\n"

#: ../adduser:928
#, perl-format
msgid "Creating home directory `%s' ..."
msgstr "Criando diretório pessoal '%s' ..."

#: ../adduser:931
#, perl-format
msgid "Couldn't create home directory `%s': %s."
msgstr "Não foi possível criar diretório pessoal '%s': %s."

#: ../adduser:945
#, perl-format
msgid "Copying files from `%s' ..."
msgstr "Copiando arquivos de '%s' ..."

#: ../adduser:948
#, perl-format
msgid "fork for `find' failed: %s"
msgstr "fork para 'find' falhou: %s"

#: ../adduser:1065
#, perl-format
msgid "The user `%s' already exists, and is not a system user."
msgstr "O usuário '%s' já existe, e não é um usuário de sistema."

#: ../adduser:1068
#, perl-format
msgid "The user `%s' already exists."
msgstr "O usuário '%s' já existe."

#: ../adduser:1073
#, perl-format
msgid "The UID %d is already in use."
msgstr "O UID %d já está em uso."

#: ../adduser:1085
#, perl-format
msgid "The GID %d is already in use."
msgstr "O GID %d já está em uso."

#: ../adduser:1095
#, perl-format
msgid "The GID %d does not exist."
msgstr "O GID %d não existe."

#: ../adduser:1162
#, fuzzy, perl-format
#| msgid ""
#| "Cannot deal with %s.\n"
#| "It is not a dir, file, or symlink.\n"
msgid "%s/%s is neither a dir, file, nor a symlink."
msgstr ""
"Não é possível lidar com %s.\n"
"Não é um diretório, arquivo ou link simbólico.\n"

#. this check cannot be turned off
#: ../adduser:1196
msgid ""
"To avoid ambiguity with numerical UIDs, usernames which\n"
"            consist of only digits are not allowed."
msgstr ""

#. this check cannot be turned off
#: ../adduser:1203
msgid ""
"Usernames must be no more than 32 bytes in length;\n"
"            note that if you are using Unicode characters, the character\n"
"            limit will be less than 32."
msgstr ""

#. this check cannot be turned off
#: ../adduser:1211
msgid ""
"To avoid problems, the username must not start with a\n"
"            dash, plus sign, or tilde, and it must not contain any of the\n"
"            following: colon, comma, slash, or any whitespace characters\n"
"            including spaces, tabs, and newlines."
msgstr ""

#: ../adduser:1221
#, fuzzy
#| msgid ""
#| "%s: To avoid problems, the username should consist only of\n"
#| "letters, digits, underscores, periods, at signs and dashes, and not start "
#| "with\n"
#| "a dash (as defined by IEEE Std 1003.1-2001). For compatibility with "
#| "Samba\n"
#| "machine accounts $ is also supported at the end of the username\n"
msgid ""
"To avoid problems, the username should consist only of\n"
"            letters, digits, underscores, periods, at signs and dashes, and\n"
"            not start with a dash (as defined by IEEE Std 1003.1-2001). For\n"
"            compatibility with Samba machine accounts, $ is also supported\n"
"            at the end of the username.  (Use the `--allow-all-names' "
"option\n"
"            to bypass this restriction.)"
msgstr ""
"%s: Para evitar problemas, o nome de usuário deve consistir somente de\n"
"letras, dígitos, underscores, pontos, arrobas e hífens, e não iniciar com "
"um\n"
"hífen (como definido por IEEE Std 1003.1-2001). Para compatibilidade\n"
"com contas de máquinas Samba o uso do caractere $ também é\n"
"suportado no final do nome do usuário\n"

#: ../adduser:1231
msgid "Allowing use of questionable username."
msgstr "Permitindo o uso de nome de usuário questionável."

#: ../adduser:1233
#, fuzzy, perl-format
#| msgid ""
#| "%s: Please enter a username matching the regular expression configured\n"
#| "via the NAME_REGEX configuration variable.  Use the `--force-badname'\n"
#| "option to relax this check or reconfigure NAME_REGEX.\n"
msgid ""
"Please enter a username matching the regular expression\n"
"            configured via the %s configuration variable.  Use the\n"
"            `--allow-bad-names' option to relax this check or reconfigure\n"
"            %s in configuration."
msgstr ""
"%s: Por favor, informe um nome de usuário compatível com a expressão\n"
"regular configurada através da variável de configuração NAME_REGEX. Use\n"
"a opção '--force-badname' para aliviar esta verificação ou reconfigure\n"
"NAME_REGEX.\n"

#: ../adduser:1254
#, fuzzy, perl-format
#| msgid "Selecting UID from range %d to %d ..."
msgid "Selecting UID from range %d to %d ...\n"
msgstr "Selecionando UID da faixa %d a %d ..."

#: ../adduser:1277
#, perl-format
msgid "Selecting GID from range %d to %d ..."
msgstr "Selecionando GID da faixa %d a %d ..."

#: ../adduser:1301
#, fuzzy, perl-format
#| msgid "Selecting UID from range %d to %d ..."
msgid "Selecting UID/GID from range %d to %d ..."
msgstr "Selecionando UID da faixa %d a %d ..."

#: ../adduser:1342
#, perl-format
msgid "Removing directory `%s' ..."
msgstr "Removendo diretório '%s' ..."

#: ../adduser:1346 ../deluser:393
#, perl-format
msgid "Removing user `%s' ..."
msgstr "Removendo usuário '%s' ..."

#. groupdel will error out if there are users left that
#. have $group as primary group. We are not checking this
#. ourself since this would mean enumerating all users.
#: ../adduser:1350 ../deluser:435
#, perl-format
msgid "Removing group `%s' ..."
msgstr "Removendo grupo '%s' ..."

#. Translators: the variable %s is INT, QUIT, or HUP.
#. Please do not insert a space character between SIG and %s.
#: ../adduser:1360
#, perl-format
msgid "Caught a SIG%s."
msgstr "Capturou um SIG%s."

#: ../adduser:1366
#, perl-format
msgid ""
"adduser version %s\n"
"\n"
msgstr ""
"adduser versão %s\n"
"\n"

#: ../adduser:1367
msgid ""
"Adds a user or group to the system.\n"
"\n"
"For detailed copyright information, please refer to\n"
"/usr/share/doc/adduser/copyright.\n"
"\n"
msgstr ""

#: ../adduser:1373 ../deluser:495
msgid ""
"This program is free software; you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation; either version 2 of the License, or (at\n"
"your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful, but\n"
"WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n"
"General Public License, /usr/share/common-licenses/GPL, for more details.\n"
msgstr ""
"Este programa é software livre; você pode redistribuí-lo e/ou\n"
"modificá-lo sob os termos da Licença Pública Geral GNU conforme\n"
"publicada pela Free Software Foundation; tanto a versão 2 da Licença,\n"
"como (a seu critério) qualquer versão posterior.\n"
"\n"
"Este programa é distribuído na expectativa de que seja útil, porém,\n"
"SEM NENHUMA GARANTIA; nem mesmo a garantia implícita de COMERCIABILIDADE\n"
"OU ADEQUAÇÃO A UMA FINALIDADE ESPECÍFICA. Consulte a Licença Pública\n"
"Geral do GNU, /usr/share/common-licenses/GPL, para mais detalhes.\n"

#: ../adduser:1387
msgid ""
"adduser [--uid id] [--firstuid id] [--lastuid id]\n"
"        [--gid id] [--firstgid id] [--lastgid id] [--ingroup group]\n"
"        [--add-extra-groups] [--shell shell]\n"
"        [--comment comment] [--home dir] [--no-create-home]\n"
"        [--allow-all-names] [--allow-bad-names]\n"
"        [--disabled-password] [--disabled-login]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        user\n"
"    Add a normal user\n"
"\n"
"adduser --system\n"
"        [--uid id] [--group] [--ingroup group] [--gid id]\n"
"        [--shell shell] [--comment comment] [--home dir] [--no-create-home]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        user\n"
"   Add a system user\n"
"\n"
"adduser --group\n"
"        [--gid ID] [--firstgid id] [--lastgid id]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        group\n"
"addgroup\n"
"        [--gid ID] [--firstgid id] [--lastgid id]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        group\n"
"   Add a user group\n"
"\n"
"addgroup --system\n"
"        [--gid id]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        group\n"
"   Add a system group\n"
"\n"
"adduser USER GROUP\n"
"   Add an existing user to an existing group\n"
msgstr ""

#: ../deluser:125
msgid "Only root may remove a user or group from the system."
msgstr "Somente root pode remover um usuário ou grupo do sistema."

#: ../deluser:158
msgid "No options allowed after names."
msgstr "Opções não são permitidas depois de nomes."

#: ../deluser:167
msgid "Enter a group name to remove: "
msgstr "Informe um nome de grupo para remover: "

#: ../deluser:169
msgid "Enter a user name to remove: "
msgstr "Informe um nome de usuário para remover: "

#: ../deluser:212
#, fuzzy
#| msgid ""
#| "In order to use the --remove-home, --remove-all-files, and --backup "
#| "features,\n"
#| "you need to install the `perl' package. To accomplish that, run\n"
#| "apt-get install perl.\n"
msgid ""
"In order to use the --remove-home, --remove-all-files, and --backup "
"features, you need to install the `perl' package. To accomplish that, run "
"apt-get install perl."
msgstr ""
"Para que seja possível utilizar as opções --remove-home, --remove-all-files "
"e\n"
"--backup, você precisa instalar o pacote 'perl'. Para fazê-lo, execute\n"
"apt-get install perl.\n"

#: ../deluser:256
#, perl-format
msgid "The user `%s' is not a system user. Exiting."
msgstr "O usuário '%s' não é um usuário de sistema. Saindo."

#: ../deluser:260
#, perl-format
msgid "The user `%s' does not exist, but --system was given. Exiting."
msgstr "O usuário '%s' não existe, mas a opção --system foi passada. Saindo."

#: ../deluser:272
msgid ""
"WARNING: You are just about to delete the root account (uid 0). Usually this "
"is never required as it may render the whole system unusable. If you really "
"want this, call deluser with parameter --no-preserve-root. Stopping now "
"without having performed any action"
msgstr ""

#: ../deluser:283
msgid "Looking for files to backup/remove ..."
msgstr "Procurando por arquivos para realizar backup/remover ..."

#: ../deluser:288
#, perl-format
msgid "failed to open /proc/mounts: %s"
msgstr ""

#: ../deluser:301
#, perl-format
msgid "failed to close /proc/mounts: %s"
msgstr ""

#: ../deluser:326
#, perl-format
msgid "Not backing up/removing `%s', it is a mount point."
msgstr "Não realizando backup/removendo '%s', ele é um ponto de montagem."

#: ../deluser:333
#, perl-format
msgid "Not backing up/removing `%s', it matches %s."
msgstr "Não realizando backup/removendo '%s', ele combina com %s."

#: ../deluser:347
#, perl-format
msgid "Cannot handle special file %s"
msgstr "Não pode manipular o arquivo especial %s"

#: ../deluser:355
#, perl-format
msgid "Backing up files to be removed to %s ..."
msgstr "Realizando backup de arquivos a serem removidos para %s ..."

#: ../deluser:374
msgid "Removing files ..."
msgstr "Removendo arquivos ..."

#: ../deluser:383
msgid "Removing crontab ..."
msgstr "Removendo crontab ..."

#: ../deluser:389
#, perl-format
msgid "`%s' not executed. Skipping crontab removal. Package `cron' required."
msgstr ""

#: ../deluser:418
#, perl-format
msgid "getgrnam `%s' failed. This shouldn't happen."
msgstr "getgrnam '%s' falhou. Isto não deveria acontecer."

#: ../deluser:424
#, perl-format
msgid "The group `%s' is not a system group. Exiting."
msgstr "O grupo '%s' não é um grupo de sistema. Saindo."

#: ../deluser:428
#, perl-format
msgid "The group `%s' is not empty!"
msgstr "O grupo '%s' não está vazio!"

#: ../deluser:447
#, fuzzy, perl-format
#| msgid "The user `%s' does not exist."
msgid "The user `%s' does not exist.\n"
msgstr "O usuário '%s' não existe."

#: ../deluser:451
#, fuzzy, perl-format
#| msgid "The group `%s' does not exist."
msgid "The group `%s' does not exist.\n"
msgstr "O grupo '%s' não existe."

#: ../deluser:455
msgid "You may not remove the user from their primary group."
msgstr "Você não pode remover o usuário de seu grupo primário."

#: ../deluser:473
#, perl-format
msgid "The user `%s' is not a member of group `%s'."
msgstr "O usuário '%s' não é um membro do grupo '%s'."

#: ../deluser:477
#, perl-format
msgid "Removing user `%s' from group `%s' ..."
msgstr "Removendo usuário '%s' do grupo '%s' ..."

#: ../deluser:488
#, perl-format
msgid ""
"deluser version %s\n"
"\n"
msgstr ""
"deluser versão %s\n"
"\n"

#: ../deluser:489
msgid ""
"Removes users and groups from the system.\n"
"\n"
"For detailed copyright information, please refer to\n"
"/usr/share/doc/adduser/copyright.\n"
"\n"
msgstr ""

#: ../deluser:509
msgid ""
"deluser [--system] [--remove-home] [--remove-all-files] [--backup]\n"
"        [--backup-to dir] [--backup-suffix str] [--conf file]\n"
"        [--quiet] [--verbose] [--debug] user\n"
"\n"
"  remove a normal user from the system\n"
"\n"
"deluser --group [--system] [--only-if-empty] [--conf file] [--quiet]\n"
"        [--verbose] [--debug] group\n"
"delgroup [--system] [--only-if-empty] [--conf file] [--quiet]\n"
"         [--verbose] [--debug] group\n"
"  remove a group from the system\n"
"\n"
"deluser [--conf file] [--quiet] [--verbose] [--debug] user group\n"
"  remove the user from a group\n"
msgstr ""

#. compressor recognized, not available
#: ../deluser:547 ../deluser:557
#, perl-format
msgid "Backup suffix %s unavailable, using gzip."
msgstr ""

#: ../AdduserCommon.pm:77
#, perl-format
msgid "`%s' does not exist. Using defaults."
msgstr "'%s' não existe. Usando padrões."

#: ../AdduserCommon.pm:83
#, perl-format
msgid "cannot open configuration file %s: `%s'\n"
msgstr ""

#: ../AdduserCommon.pm:91 ../AdduserCommon.pm:155 ../AdduserCommon.pm:168
#, perl-format
msgid "Couldn't parse `%s', line %d."
msgstr "Não foi possível verificar '%s', linha %d."

#: ../AdduserCommon.pm:96
#, perl-format
msgid "Unknown variable `%s' at `%s', line %d."
msgstr "Variável '%s' desconhecida em '%s', linha %d."

#: ../AdduserCommon.pm:122
#, fuzzy, perl-format
#| msgid "Not creating home directory `%s'."
msgid "Cannot read directory `%s'"
msgstr "Não criando diretório pessoal '%s'."

#: ../AdduserCommon.pm:137
#, fuzzy, perl-format
#| msgid "The user `%s' does not exist."
msgid "`%s' does not exist."
msgstr "O usuário '%s' não existe."

#: ../AdduserCommon.pm:142
#, fuzzy, perl-format
#| msgid "Cannot handle special file %s"
msgid "Cannot open pool file %s: `%s'"
msgstr "Não pode manipular o arquivo especial %s"

#: ../AdduserCommon.pm:175
#, perl-format
msgid "Illegal pool type `%s' reading `%s'."
msgstr ""

#: ../AdduserCommon.pm:179
#, fuzzy, perl-format
#| msgid "Unknown variable `%s' at `%s', line %d."
msgid "Duplicate name `%s' at `%s', line %d."
msgstr "Variável '%s' desconhecida em '%s', linha %d."

#: ../AdduserCommon.pm:183
#, fuzzy, perl-format
#| msgid "Unknown variable `%s' at `%s', line %d."
msgid "Duplicate ID `%s' at `%s', line %d."
msgstr "Variável '%s' desconhecida em '%s', linha %d."

#: ../AdduserCommon.pm:213
#, perl-format
msgid "`%s' returned error code %d. Exiting."
msgstr "'%s' retornou código de erro %d. Saindo."

#: ../AdduserCommon.pm:216
#, perl-format
msgid "`%s' exited from signal %d. Exiting."
msgstr "'%s' saiu a partir do sinal %d. Saindo."

#: ../AdduserCommon.pm:227
#, perl-format
msgid "`%s' failed to execute. %s. Continuing."
msgstr ""

#: ../AdduserCommon.pm:229
#, fuzzy, perl-format
#| msgid "`%s' exited from signal %d. Exiting."
msgid "`%s' killed by signal %d. Continuing."
msgstr "'%s' saiu a partir do sinal %d. Saindo."

#: ../AdduserCommon.pm:231
#, perl-format
msgid "`%s' failed with status %d. Continuing."
msgstr ""

#: ../AdduserCommon.pm:270
#, perl-format
msgid "Could not find program named `%s' in $PATH."
msgstr "Não foi possível encontrar o programa de nome '%s' em $PATH."

#: ../AdduserCommon.pm:343
#, perl-format
msgid "could not open lock file %s!"
msgstr ""

#: ../AdduserCommon.pm:349
msgid "Could not obtain exclusive lock, please try again shortly!"
msgstr ""

#: ../AdduserCommon.pm:352
msgid "Waiting for lock to become available..."
msgstr ""

#: ../AdduserCommon.pm:359
#, perl-format
msgid "could not seek - %s!"
msgstr ""

#: ../AdduserCommon.pm:368
msgid "could not find lock file!"
msgstr ""

#: ../AdduserCommon.pm:373
#, perl-format
msgid "could not unlock file %s: %s"
msgstr ""

#: ../AdduserCommon.pm:378
#, perl-format
msgid "could not close lock file %s: %s"
msgstr ""

#~ msgid "Done."
#~ msgstr "Concluído."

#, perl-format
#~ msgid "Stopped: %s"
#~ msgstr "Parou: %s"

#~ msgid "WARNING: You are just about to delete the root account (uid 0)"
#~ msgstr "ALERTA: você está prestes a remover a conta root (uid 0)"

#~ msgid ""
#~ "Usually this is never required as it may render the whole system "
#~ "unusable\n"
#~ msgstr ""
#~ "Geralmente, isto nunca é requerido, pois pode tornar todo o sistema "
#~ "inutilizável\n"

#, fuzzy
#~| msgid "If you really want this, call deluser with parameter --force"
#~ msgid ""
#~ "If you really want this, call deluser with parameter --no-preserve-root\n"
#~ msgstr ""
#~ "Se você realmente quer isto, execute deluser com o parâmetro --force"

#~ msgid "Stopping now without having performed any action"
#~ msgstr "Parando agora sem ter executado nenhuma ação"

#, perl-format
#~ msgid "%s: %s"
#~ msgstr "%s: %s"

#, perl-format
#~ msgid "No UID is available in the range %d-%d (FIRST_UID - LAST_UID)."
#~ msgstr "Nenhum UID está disponível na faixa %d-%d (FIRST_UID - LAST_UID)."

#, fuzzy
#~| msgid ""
#~| "Adds a user or group to the system.\n"
#~| "  \n"
#~| "Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>\n"
#~| "Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,\n"
#~| "                   Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
#~| "\n"
#~ msgid ""
#~ "Adds a user or group to the system.\n"
#~ "\n"
#~ "Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>\n"
#~ "Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,\n"
#~ "                   Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
#~ "\n"
#~ msgstr ""
#~ "Adiciona um usuário ou grupo ao sistema.\n"
#~ "  \n"
#~ "Copyright (C) 1997, 1998, 1999 Guy Maor <maor@debian.org>\n"
#~ "Copyright (C) 1995 Ian Murdock <imurdock@gnu.ai.mit.edu>,\n"
#~ "                   Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
#~ "\n"

#, fuzzy
#~| msgid ""
#~| "adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]\n"
#~| "[--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GROUP | --gid "
#~| "ID]\n"
#~| "[--disabled-password] [--disabled-login] [--add_extra_groups] USER\n"
#~| "   Add a normal user\n"
#~| "\n"
#~| "adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid "
#~| "ID]\n"
#~| "[--gecos GECOS] [--group | --ingroup GROUP | --gid ID] [--disabled-"
#~| "password]\n"
#~| "[--disabled-login] [--add_extra_groups] USER\n"
#~| "   Add a system user\n"
#~| "\n"
#~| "adduser --group [--gid ID] GROUP\n"
#~| "addgroup [--gid ID] GROUP\n"
#~| "   Add a user group\n"
#~| "\n"
#~| "addgroup --system [--gid ID] GROUP\n"
#~| "   Add a system group\n"
#~| "\n"
#~| "adduser USER GROUP\n"
#~| "   Add an existing user to an existing group\n"
#~| "\n"
#~| "general options:\n"
#~| "  --quiet | -q      don't give process information to stdout\n"
#~| "  --force-badname   allow usernames which do not match the\n"
#~| "                    NAME_REGEX configuration variable\n"
#~| "  --help | -h       usage message\n"
#~| "  --version | -v    version number and copyright\n"
#~| "  --conf | -c FILE  use FILE as configuration file\n"
#~| "\n"
#~ msgid ""
#~ "adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]\n"
#~ "[--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GROUP | --gid "
#~ "ID]\n"
#~ "[--disabled-password] [--disabled-login] [--add_extra_groups] USER\n"
#~ "   Add a normal user\n"
#~ "\n"
#~ "adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid "
#~ "ID]\n"
#~ "[--gecos GECOS] [--group | --ingroup GROUP | --gid ID] [--disabled-"
#~ "password]\n"
#~ "[--disabled-login] [--add_extra_groups] USER\n"
#~ "   Add a system user\n"
#~ "\n"
#~ "adduser --group GROUP\n"
#~ "addgroup [--gid=GID] GROUP\n"
#~ "   Add a user group\n"
#~ "\n"
#~ "addgroup --system [--gid=GID] GROUP\n"
#~ "   Add a system group\n"
#~ "\n"
#~ "adduser USER GROUP\n"
#~ "   Add an existing user to an existing group\n"
#~ "\n"
#~ "general options:\n"
#~ "  --force-badname       allow usernames which do not match the\n"
#~ "                        NAME_REGEX configuration variable\n"
#~ "  -q, --quiet           don't give process information to stdout\n"
#~ "  -d, --debug           be more verbose during execution\n"
#~ "  -h, --help            usage message\n"
#~ "  -v, --version         version number and copyright\n"
#~ "  -c FILE, --conf=FILE  use FILE as configuration file\n"
#~ "\n"
#~ msgstr ""
#~ "adduser [--home DIR] [--shell SHELL] [--no-create-home] [--uid ID]\n"
#~ "[--firstuid ID] [--lastuid ID] [--gecos GECOS] [--ingroup GROUP | --gid "
#~ "ID]\n"
#~ "[--disabled-password] [--disabled-login] [--add_extra_groups] USUÁRIO\n"
#~ "   Adiciona um usuário normal\n"
#~ "\n"
#~ "adduser --system [--home DIR] [--shell SHELL] [--no-create-home] [--uid "
#~ "ID]\n"
#~ "[--gecos GECOS] [--group | --ingroup GRUPO | --gid ID] [--disabled-"
#~ "password]\n"
#~ "[--disabled-login] [--add_extra_groups] USUÁRIO\n"
#~ "   Adiciona um usuário de sistema\n"
#~ "\n"
#~ "adduser --group [--gid ID] GRUPO\n"
#~ "addgroup [--gid ID] GRUPO\n"
#~ "   Adiciona um grupo de usuário\n"
#~ "\n"
#~ "addgroup --system [--gid ID] GRUPO\n"
#~ "   Adiciona um grupo de sistema\n"
#~ "\n"
#~ "adduser USUÁRIO GRUPO\n"
#~ "   Adiciona um usuário existente a um grupo existente\n"
#~ "\n"
#~ "opções gerais:\n"
#~ "  --quiet | -q      não passa informações de processo para stdout\n"
#~ "  --force-badname   permite nomes de usuário que não combinam com\n"
#~ "                    a variável de configuração NAME_REGEX\n"
#~ "  --help | -h       mensagem de utilização\n"
#~ "  --version | -v    número de versão e copyright\n"
#~ "  --conf | -c FILE  usa ARQUIVO como arquivo de configuração\n"
#~ "\n"

#, perl-format
#~ msgid "fork for `mount' to parse mount points failed: %s"
#~ msgstr "fork para 'mount' para analisar os pontos de montagem falhou: %s"

#, perl-format
#~ msgid "pipe of command `mount' could not be closed: %s"
#~ msgstr "pipe do comando 'mount' não pôde ser fechado: %s"

#, perl-format
#~ msgid "`%s' still has `%s' as their primary group!"
#~ msgstr "'%s' ainda tem '%s' como seu grupo primário!"

#~ msgid "Removes users and groups from the system."
#~ msgstr "Remove usuários e grupos do sistema."

#~ msgid ""
#~ "Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>\n"
#~ "\n"
#~ msgstr ""
#~ "Copyright (C) 2000 Roland Bauerschmidt <roland@copyleft.de>\n"
#~ "\n"

#~ msgid ""
#~ "deluser is based on adduser by Guy Maor <maor@debian.org>, Ian Murdock\n"
#~ "<imurdock@gnu.ai.mit.edu> and Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
#~ "\n"
#~ msgstr ""
#~ "deluser é baseado em adduser por Guy Maor <maor@debian.org>, Ian Murdock\n"
#~ "<imurdock@gnu.ai.mit.edu> e Ted Hajek <tedhajek@boombox.micro.umn.edu>\n"
#~ "\n"

#, fuzzy
#~| msgid ""
#~| "deluser USER\n"
#~| "  remove a normal user from the system\n"
#~| "  example: deluser mike\n"
#~| "\n"
#~| "  --remove-home             remove the users home directory and mail "
#~| "spool\n"
#~| "  --remove-all-files        remove all files owned by user\n"
#~| "  --backup                  backup files before removing.\n"
#~| "  --backup-to <DIR>         target directory for the backups.\n"
#~| "                            Default is the current directory.\n"
#~| "  --system                  only remove if system user\n"
#~| "\n"
#~| "delgroup GROUP\n"
#~| "deluser --group GROUP\n"
#~| "  remove a group from the system\n"
#~| "  example: deluser --group students\n"
#~| "\n"
#~| "  --system                  only remove if system group\n"
#~| "  --only-if-empty           only remove if no members left\n"
#~| "\n"
#~| "deluser USER GROUP\n"
#~| "  remove the user from a group\n"
#~| "  example: deluser mike students\n"
#~| "\n"
#~| "general options:\n"
#~| "  --quiet | -q      don't give process information to stdout\n"
#~| "  --help | -h       usage message\n"
#~| "  --version | -v    version number and copyright\n"
#~| "  --conf | -c FILE  use FILE as configuration file\n"
#~| "\n"
#~ msgid ""
#~ "deluser USER\n"
#~ "  remove a normal user from the system\n"
#~ "  example: deluser mike\n"
#~ "\n"
#~ "  --remove-home             remove the users home directory and mail "
#~ "spool\n"
#~ "  --remove-all-files        remove all files owned by user\n"
#~ "  --backup                  backup files before removing.\n"
#~ "  --backup-to <DIR>         target directory for the backups.\n"
#~ "                            Default is the current directory.\n"
#~ "  --system                  only remove if system user\n"
#~ "\n"
#~ "delgroup GROUP\n"
#~ "deluser --group GROUP\n"
#~ "  remove a group from the system\n"
#~ "  example: deluser --group students\n"
#~ "\n"
#~ "  --system                  only remove if system group\n"
#~ "  --only-if-empty           only remove if no members left\n"
#~ "\n"
#~ "deluser USER GROUP\n"
#~ "  remove the user from a group\n"
#~ "  example: deluser mike students\n"
#~ "\n"
#~ "general options:\n"
#~ "  -q, --quiet           don't give process information to stdout\n"
#~ "  -d, --debug           be more verbose\n"
#~ "  -h, --help            usage message\n"
#~ "  -v, --version         version number and copyright\n"
#~ "  -c FILE, --conf=FILE  use FILE as configuration file\n"
#~ "\n"
#~ msgstr ""
#~ "deluser USUÁRIO\n"
#~ "  remove um usuário normal do sistema\n"
#~ "  exemplo: deluser mike\n"
#~ "\n"
#~ "  --remove-home        remove o diretório pessoal e mail spool do "
#~ "usuário\n"
#~ "  --remove-all-files   remove todos os arquivos dos quais o usuário é o "
#~ "dono\n"
#~ "  --backup             realiza backup de arquivos antes de remover.\n"
#~ "  --backup-to <DIR>    diretório de destino para os backups.\n"
#~ "                       O padrão é o diretório corrente.\n"
#~ "  --system             remove somente se for usuário de sistema\n"
#~ "\n"
#~ "delgroup GRUPO\n"
#~ "deluser --group GRUPO\n"
#~ "  remove um grupo do sistema\n"
#~ "  exemplo: deluser --group students\n"
#~ "\n"
#~ "  --system             remove somente se for grupo de sistema\n"
#~ "  --only-if-empty      remove somente se não houver membros restantes\n"
#~ "\n"
#~ "deluser USUÁRIO GRUPO\n"
#~ "  remove o usuário de um grupo\n"
#~ "  exemplo: deluser mike students\n"
#~ "\n"
#~ "opções gerais:\n"
#~ "  --quiet | -q         não passa informações de processo para stdout\n"
#~ "  --help | -h          mensagem de utilização\n"
#~ "  --version | -v       número da versão e copyright\n"
#~ "  --conf | -c ARQUIVO  usa ARQUIVO como arquivo de configuração\n"
#~ "\n"

#, perl-format
#~ msgid "The user `%s' already exists. Exiting."
#~ msgstr "O usuário '%s' já existe. Saindo."
